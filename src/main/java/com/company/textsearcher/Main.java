/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.textsearcher;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @author danil
 *
 * <p>
 * Обход файловой системы и поиск заданного текста в файловой системе.
 * Приложение многопоточное, запускается из командной строки и принимает
 * аргумент как параметр поиска.
 * </p>
 *
 * Поиск осуществляется посредством выполнения метода 
 * private void multithreadedSearch(String path, String text, List<String> fileResolutions)
 * String path - заданный путь, с которого осуществляется обход 
 * String text - введенный текст для поиска 
 * List<String> fileResolutions - список разрешений файлов (данный список конкретизирует область поиска заданного текста)
 *
 * Выполненная программа вывод в консоль список файлов, в который был найден
 * заданный текст, а также номер строки с каждым совпадание, и список директорий
 * (файлов), содержащих в своем названии заданный текст.
 *
 * <p>
 * Логика приложения 
 * 1. Ввод текста для поиска в файловой системе 
 * 2. Поиск файлов, удовлетворяющим допустимым разрешениям и содержащих в названии заданный текст 
 * 3. Поиск текста в файлах с допустимым разрешением 
 * 4. Вывод результатов поиска
 * </p>
 *
 * <p>
 * Запуск программы из консоли 
 * 1. Выполнить переход в следующую дикреторию textSearcher/target 
 * 2. Выполнить команду java -jar textSearcher-1.0.jar text 
 *    text - текст, вводимый для поиска
 * </p>
 */
public class Main {

    private final List<String> resultFilesList = new CopyOnWriteArrayList<>();

    /**
     * Список файлов, удовлетворяющих условию допустимых разрешений для
     * просмотра
     */
    private final List<String> filesList = new ArrayList<>();

    /**
     * Список файлов, название которых содержит заданный текст
     */
    private final List<String> fileWithSearchName = new ArrayList<>();

    /**
     * Главный метод класса {@link Main}
     *
     * @param args String массив строковых данных, передаваемых в главный метод
     */
    public static void main(String[] args) {
        if (args.length > 0) {
            StringBuilder text = new StringBuilder();
            for (int i = 0; i < args.length; i++) {
                text.append(args[i]);
                if (i != args.length - 1) {
                    text.append(" ");
                }
            }
            Main textSearcher = new Main();
            String path = "/home/danil/projects/textSearcher";
            List<String> fileResolutions = new ArrayList<>();
            fileResolutions.add(".txt");
            fileResolutions.add(".java");
            textSearcher.multithreadedSearch(path, text.toString(), fileResolutions);
            textSearcher.getResult(text.toString());
        } else {
            System.out.println("Текст для поиска не введен");
        }
    }

    /**
     * Многопоточный поиск текста внутри файлов
     *
     * @param path String стартовая директория
     * @param text String заданный для поиска текст
     * @param fileResolutions List<String> список разрешений файлов
     */
    private void multithreadedSearch(String path, String text, List<String> fileResolutions) {
        searchInDir(path, text, fileResolutions);
        Thread firstThread = new Thread(() -> {
            for (int i = 0; i < filesList.size() / 2; i++) {
                searchInFile(filesList.get(i), text);
            }
        });
        Thread secondThread = new Thread(() -> {
            for (int i = filesList.size() / 2; i < filesList.size(); i++) {
                searchInFile(filesList.get(i), text);
            }
        });
        firstThread.start();
        secondThread.start();
        try {
            firstThread.join();
            secondThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод обхода директорий по заданному пути
     *
     * @param path String стартовая директория
     * @param text String заданный для поиска текст
     * @param fileResolutions List<String> список разрешений файлов
     */
    private void searchInDir(String path, String text, List<String> fileResolutions) {
        File file = new File(path);
        try {
            for (File currentFile : file.listFiles()) {
                String name = currentFile.getName();
                if (name.contains(text)) {
                    this.fileWithSearchName.add(currentFile.getAbsolutePath());
                }
                if (currentFile.isDirectory()) {
                    searchInDir(currentFile.getAbsolutePath(), text, fileResolutions);
                } else {
//                    System.out.println(currentFile.getName());
                    fileResolutions.stream().filter((resolution) -> (currentFile.getName().endsWith(resolution))).forEachOrdered((_item) -> {
                        this.filesList.add(currentFile.getAbsolutePath());
                    });
                }
            }
        } catch (NullPointerException e) {
            System.out.println("Неверный путь: " + file);
            e.printStackTrace();
        }
    }

    /**
     * Метод поиска заданного текста внутри файлов
     *
     * @param path String стартовая директория
     * @param text String заданный для поиска текст
     */
    private void searchInFile(final String path, final String text) {
        File file = new File(path);
        Scanner sc;
        try {
            sc = new Scanner(file);
            try {
                int i = 0;
                while (sc.hasNextLine()) {
                    i++;
                    if (sc.nextLine().contains(text)) {
                        this.resultFilesList.add(file.getAbsolutePath() + " Номер строки:" + i);
                    }
                }
            } finally {
                sc.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод поиска заданного текста внутри файлов
     *
     * @param text String заданный для поиска текст
     */
    private void getResult(String text) {
        if (resultFilesList.isEmpty() && fileWithSearchName.isEmpty()) {
            System.out.println("К сожалению, по вашему запросу ничего не найдено");
        } else {
            System.out.println("Найдено " + resultFilesList.size() + " файлов, содержащих заданный текст(" + text + ")");
            resultFilesList.forEach((file) -> {
                System.out.println("Абсолютный путь файла:" + file);
            });
            if (!fileWithSearchName.isEmpty()) {
                System.out.println("Найдено " + fileWithSearchName.size() + " файлов, включающих в свое название заданный текст(" + text + ")");
                fileWithSearchName.forEach((file) -> {
                    System.out.println("Абсолютный путь файла:" + file);
                });
            }
        }
    }
}
